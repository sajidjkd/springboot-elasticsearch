# Sping data elastic search with LogStash


## 1. How to start
```
$ git clone https://sajidjkd@bitbucket.org/sajidjkd/springboot-elasticsearch.git
$ cd springboot-elasticsearch
$ mvn spring-boot:run

$ curl -v localhost:8080/customers
```

## 2. Stepup Elastic Search



###mac:
```
curl -L -O https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.4.2-darwin-x86_64.tar.gz
tar -xzvf elasticsearch-7.4.2-darwin-x86_64.tar.gz
cd elasticsearch-7.4.2
./bin/elasticsearch
```
###brew:
```
brew tap elastic/tap
brew install elastic/tap/elasticsearch-full
elasticsearch
```
###linux:
```
curl -L -O https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.4.2-linux-x86_64.tar.gz
tar -xzvf elasticsearch-7.4.2-linux-x86_64.tar.gz
cd elasticsearch-7.4.2
./bin/elasticsearch
```

###Start ES
```
./bin/elasticsearch
```

###Default Configuration
```
The default cluster.name and node.name are elasticsearch and your hostname, respectively.
 If you plan to keep using this cluster or add more nodes, it is a good idea to change these default values to unique names.
  For details about changing these and other settings in the elasticsearch.yml (config/elasticsearch.yml) file,
  see Configuring Elasticsearch. (https://www.elastic.co/guide/en/elasticsearch/reference/7.4/settings.html)
```


## LogStash

###Install Logstash


```
##mac and linux:
```
curl -L -O https://artifacts.elastic.co/downloads/logstash/logstash-7.4.2.tar.gz
tar -xzvf logstash-7.4.2.tar.gz
```
##brew:
```
brew tap elastic/tap
brew install elastic/tap/logstash-full
```

### Configure LogStash
```1. Create logstash-sync.conf file and place in bin folder of logstash installation
   2. Download mysql connector jar  ex : mysql-connector-java-5.1.48-bin.jar file and copy to folder  logstash/logstash-core/lib/jars/ 
```
```
input {
  jdbc {
    jdbc_driver_library => ""
    jdbc_driver_class => "com.mysql.jdbc.Driver"
    jdbc_connection_string => "jdbc:mysql://localhost:3306/ecomdb"
    jdbc_user => "root"
    jdbc_password => "Password"
    tracking_column => "regdate"
    use_column_value =>true
    statement => "SELECT * FROM ecomdb.customer where regdate >:sql_last_value;"
    schedule => "*/1 * * * *"
  }
}
output {
  elasticsearch {
    document_id=> "%{id}"
    document_type => "doc"
    index => "test"
    hosts => ["http://localhost:9200"]
  }
  stdout{
  codec => rubydebug
  }
}
```



##Start logstash
```
./bin/logstash -f ./bin/logstash-sync.conf
```

##Install MySQL and run following DB scripts
```

CREATE DATABASE ecomdb;
USE ecomdb;

CREATE TABLE customer (
id INT(6)  AUTO_INCREMENT PRIMARY KEY,
firstname VARCHAR(30) NOT NULL,
lastname VARCHAR(30) NOT NULL,
email VARCHAR(50),
regdate TIMESTAMP
);
INSERT INTO `ecomdb`.`customer` (`id`, `firstname`, `lastname`, `email`, `regdate`) VALUES (1, 'Roger', 'Federer', 'roger.federer@yomail.com', '2019-01-21 20:21:49');
INSERT INTO `ecomdb`.`customer` (`id`, `firstname`, `lastname`, `email`, `regdate`) VALUES (2, 'Rafael', 'Nadal', 'rafael.nadal@yomail.com', '2019-01-22 20:21:49');
INSERT INTO `ecomdb`.`customer` (`id`, `firstname`, `lastname`, `email`, `regdate`) VALUES (3, 'John', 'Mcenroe', 'john.mcenroe@yomail.com', '2019-01-23 20:21:49'); 
INSERT INTO `ecomdb`.`customer` (`id`, `firstname`, `lastname`, `email`, `regdate`) VALUES (4, 'Ivan', 'Lendl', 'ivan.lendl@yomail.com', '2019-01-23 23:21:49'); 
INSERT INTO `ecomdb`.`customer` (`id`, `firstname`, `lastname`, `email`, `regdate`) VALUES (5, 'Jimmy', 'Connors', 'jimmy.connors@yomail.com', '2019-01-23 22:21:49');


```





