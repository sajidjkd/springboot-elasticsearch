package com.springboot.elasticsearch.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.elasticsearch.model.Customer;
import com.springboot.elasticsearch.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	/*
	 * public Page<Book> findByAuthor(String author, PageRequest pageRequest) {
	 * return bookRepository.findByAuthor(author, pageRequest); }
	 */

	public List<Customer> findByFirstname(String firstName) {
		return customerRepository.findByFirstname(firstName);
	}

	@Override
	public Iterable<Customer> findAllCustomers() {
		return customerRepository.findAll();
	}

}