package com.springboot.elasticsearch.service;

import java.util.List;

import com.springboot.elasticsearch.model.Customer;

public interface CustomerService {


    //Page<Book> findByAuthor(String author, PageRequest pageRequest);

    List<Customer> findByFirstname(String firstName);

	Iterable<Customer> findAllCustomers();

}