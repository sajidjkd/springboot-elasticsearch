package com.springboot.elasticsearch.repository;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.springboot.elasticsearch.model.Customer;

@Repository
public interface CustomerRepository extends ElasticsearchRepository<Customer, Long> {

    //Page<Customer> findByFirstName(String author, Pageable pageable);

    List<Customer> findByFirstname(String firstName);

}