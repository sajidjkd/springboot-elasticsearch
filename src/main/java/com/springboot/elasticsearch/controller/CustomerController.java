package com.springboot.elasticsearch.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.elasticsearch.model.Customer;
import com.springboot.elasticsearch.service.CustomerService;

@RestController
@RequestMapping(value= "/customer")
public class CustomerController {

	@Autowired
	CustomerService cs;	

	

	/**
	 * Method to fetch all employees from the database.
	 * @return
	 */
	@GetMapping
	public Iterable<Customer> getAllCustomers() {
		return cs.findAllCustomers();
	}

	/**
	 * Method to fetch the employee details on the basis of designation.
	 * @param designation
	 * @return
	 */
	@GetMapping(value= "/{name}")
	public List<Customer> getByDesignation(@PathVariable(name= "name") String name) {
		return cs.findByFirstname(name);
	}
}